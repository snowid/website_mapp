//index.js
var util = require("../../utils/util.js");
var trail = require("../../utils/trail.js");
const app = getApp()

Page({
    data: {
        userInfo: {},
        hasUserInfo: false,
        banners: null,
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        goods_cates: null,
        hot_news: null,
        curtarget:"product"
    },
    onLoad: function () {
        wx.showLoading({
            title: '加载中',
        })
        app.getSiteInfo(siteinfo => {
            //console.log(siteinfo)
            wx.setNavigationBarTitle({
                title: siteinfo.name,
            })
            app.initShare(this, siteinfo.name, siteinfo.logo)
        })

        app.httpPost('common/advs',{flag:'banner'}, (json) => {
            //console.log(json)
            if (json && json.data) {
                json.data = trail.fixListImage(json.data, 'image')
                this.setData({
                    banners: json.data
                })
            }
        })
    },
    onShow: function () {
        app.httpPost('common/batch',{
            'product.get_cates':{},
            'product.get_list':{},
            'article.get_list':{'cate':'news'}
        }, json => {
            if (json.code == 1) {
                let cates = json.data['product.get_cates'];
                let goods = json.data['product.get_list'];
                let news = json.data['article.get_list'];
                if(cates){
                    cates = trail.fixListImage(cates, 'icon')
                    if (cates.length % 2 == 1) cates.push({})
                }
                if (goods){
                    goods = trail.fixListImage(goods.lists, 'image')
                    if (goods.length % 2 == 1) goods.push({})
                }
                if(news){
                    news = trail.fixListImage(news.lists, 'cover')
                }
                
                this.setData({
                    goods_cates: cates,
                    goods: goods,
                    hot_news: news
                })
                wx.hideLoading()
            }
        })
    },
    changeBlock:function(e){
        var target=e.target.dataset.target
        if(target){
            this.setData({
                curtarget:target
            })
        }
    },
    gotoProductList: function (e) {
        /*wx.navigateTo({
            url: '../product/list?cate_id=' + e.currentTarget.dataset.id,
        })*/
        app.globalData.tmp_cate_id = e.currentTarget.dataset.id
        wx.switchTab({
            url: '/pages/product/index'
        })
    },
    gotoProduct: function (e) {
        wx.navigateTo({
            url: '../product/detail?id=' + e.currentTarget.dataset.id,
        })
    },
    gotoNewsList: function (e) {
        wx.navigateTo({
            url: '../news/list?cate_id=' + e.currentTarget.dataset.id,
        })
    },
    gotoNews: function (e) {
        wx.navigateTo({
            url: '../news/detail?id=' + e.currentTarget.dataset.id,
        })
    }
})
